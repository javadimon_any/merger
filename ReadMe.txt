Get source
git clone https://gitlab.com/javadimon_any/merger.git

Build & Test
cd merger
mvn clean install

Run
cd target
java -jar merger-1.0-SNAPSHOT.jar /path/to/users.json