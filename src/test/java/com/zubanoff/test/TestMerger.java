package com.zubanoff.test;

import com.zubanoff.Main;
import com.zubanoff.User;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Dmitry Zubanov aka javadimon on 17.01.19.
 */
public class TestMerger {

    @Test
    public void testMerge() throws IOException {
        Main main = new Main();
        ArrayList<User> result = main.merge(Paths.get(System.getProperty("user.dir"), "users.json").toString());

        List<User> users = new ArrayList<>();

        User user1 = new User();
        user1.setName("user1");
        ArrayList<String> emails1 = new ArrayList<>();
        Collections.addAll(emails1, "xxx@ya.ru", "foo@gmail.com", "lol@mail.ru", "ups@pisem.net", "aaa@bbb.ru");
        user1.setEmails(emails1);
        users.add(user1);

        User user3 = new User();
        user3.setName("user3");
        ArrayList<String> emails3 = new ArrayList<>();
        Collections.addAll(emails3, "xyz@pisem.net", "vasya@pupkin.com");
        user3.setEmails(emails3);
        users.add(user3);

        Assert.assertEquals(users, result);
    }
}
