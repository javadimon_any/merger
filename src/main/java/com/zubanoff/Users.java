package com.zubanoff;

import java.util.ArrayList;

/**
 * Created by Dmitry Zubanov aka javadimon on 17.01.19.
 */
public class Users{

    private ArrayList<User> users;

    public Users(){}

    public ArrayList<User> getUsers() {
        return users;
    }

    public void setUsers(ArrayList<User> users) {
        this.users = users;
    }

    @Override
    public String toString() {
        return "Users{" +
                "users=" + users +
                '}';
    }
}
