package com.zubanoff;

import com.google.gson.Gson;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class Main {

    private final Gson gson = new Gson();

    public static void main(String[] args) throws IOException {
        if(args.length == 0){
            System.out.println("Use java -jar merger.jar /path/to/users.json");
            System.exit(0);
        }

        Main main = new Main();
        main.merge(args[0].trim());
    }

    public ArrayList<User> merge(String file) throws IOException {
        ArrayList<User> users = gson.fromJson(new String(Files.readAllBytes(Paths.get(file))), Users.class).getUsers();
        Collections.sort(users);

        ArrayList<User> excludedUsers = new ArrayList<>();
        users.stream().filter(user -> Collections.binarySearch(excludedUsers, user) < 0)
                .forEachOrdered(user -> users.stream().filter(u -> !user.equals(u))
                        .forEach(u -> u.getEmails().stream().filter(email -> Collections.binarySearch(user.getEmails(), email) >= 0)
                                .forEach(email -> {
            user.getEmails().addAll(u.getEmails());
            excludedUsers.add(u);
        })));

        users.removeAll(excludedUsers);
        users.forEach(user -> {
            LinkedHashSet<String> emails = new LinkedHashSet<>();
            emails.addAll(user.getEmails());
            user.getEmails().clear();
            user.getEmails().addAll(emails);
        });
        users.forEach(System.out::println);

        return users;
    }
}
