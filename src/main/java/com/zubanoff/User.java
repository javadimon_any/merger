package com.zubanoff;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Created by Dmitry Zubanov aka javadimon on 17.01.19.
 */
public class User implements Comparable<User>{

    private String name;
    private ArrayList<String> emails;

    public User(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<String> getEmails() {
        return emails;
    }

    public void setEmails(ArrayList<String> emails) {
        this.emails = emails;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(name, user.name) &&
                Objects.equals(emails, user.emails);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, emails);
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", emails=" + emails +
                '}';
    }

    @Override
    public int compareTo(User u) {
        return this.getName().compareTo(u.getName());
    }
}
